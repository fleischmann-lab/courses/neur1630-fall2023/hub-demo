# Demo and tests for JupyterHub resources

[![Launch JupyterHub](https://img.shields.io/badge/jupyterhub-demo-orange?style=flat-square)](https://neur1630.jupyter.brown.edu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.com%2Ffleischmann-lab%2Fcourses%2Fneur1630-fall2023%2Fhub-demo&urlpath=lab%2Ftree%2Fhub-demo%2Fhub-demo.ipynb&branch=main)

Click on the icon above (or [here](https://neur1630.jupyter.brown.edu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.com%2Ffleischmann-lab%2Fcourses%2Fneur1630-fall2023%2Fhub-demo&urlpath=lab%2Ftree%2Fhub-demo%2Fhub-demo.ipynb&branch=main)) to launch Jupyter Hub.

You can also view `hub-demo.ipynb`.
